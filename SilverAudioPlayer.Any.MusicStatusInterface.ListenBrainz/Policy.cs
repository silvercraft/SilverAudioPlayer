namespace SilverAudioPlayer.Any.MusicStatusInterface.ListenBrainz;

public enum Policy
{
    Percentage,
    Seconds
}