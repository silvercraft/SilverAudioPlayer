namespace SilverCraft.Ekorensis;

public interface IConfigurableLabel: IConfigurableElement
{
    public string Content { get; }
}