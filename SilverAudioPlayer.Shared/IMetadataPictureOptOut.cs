namespace SilverAudioPlayer.Shared
{
    public interface IMetadataPictureOptOut
    {
        public void StopLoadingPictures();
    }
}
